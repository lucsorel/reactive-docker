'use strict';

var zmq = require('zmq'),
    pusher = zmq.socket('push'),
    connectionPort = 6969,
    connectionPoint = 'tcp://0.0.0.0:' + connectionPort;

let jobCount = 0;

// sends jobs to the work queue.
setInterval(function() {
    jobCount += 1;
    var message = {
            pid: process.pid,
            job: 'job' + jobCount
        },
        messageString = JSON.stringify(message);

    console.log('sending new job ' + messageString);
    pusher.send(messageString);
}, 3000);

// binds on TCP port 6969
pusher.bind(connectionPoint, function(err) {
    if (err) {
        console.log('zut: ' + err);
    }
    console.log('Listening on ' + connectionPoint + ' for zmq pullers...');
});
