import zmq
import json
import os

context = zmq.Context()

# socket to receive messages on
receiver = context.socket(zmq.PULL)
connectionPoint = 'tcp://' + os.getenv('PROBES_TCP_URL', '0.0.0.0:6969')

print('pulling from ' + connectionPoint + '...')

receiver.connect(connectionPoint)

# awaits for messages
while True:
    data = receiver.recv()
    message = json.loads(data)
    print('received job ' + str(message['pid']) + ': ' + message['job'])
