# pyzmq
[Python ZeroMQ bindings](https://github.com/zeromq/pyzmq)

## Installation
* via pip
```bash
sudo pip install pyzmq
or
sudo pip install --upgrade pyzmq
```

* Alpine Linux (official [py-zmq package](https://pkgs.alpinelinux.org/package/testing/x86/py-zmq))
```bash
sudo apk add py-zmq
```


https://imagelayers.io/?images=lucsorel/zeromq-bindings:alpinelinux3.3-zeromq4.3.x-pyzmq15.2.0



* Debian/Ubuntu
```bash
sudo apt-get install python-zmq
```

## Documentation
https://pyzmq.readthedocs.org/en/latest/

## Examples and tutorials
* [Learning ØMQ with pyzmq](https://learning-0mq-with-pyzmq.readthedocs.org/en/latest/)

# Interaction with Spark
* see [Linking with Spark](http://spark.apache.org/docs/latest/programming-guide.html#linking-with-spark), Python tab.
* [PySpark 1.6.0 documentation](http://spark.apache.org/docs/latest/api/python/)

Note: using PyPy seems faster than classic Python: [Python vs. Scala vs. Spark](http://emptypipes.org/2015/01/17/python-vs-scala-vs-spark/).

# Python and asynchrony
[asyncio – Asynchronous I/O, event loop, coroutines and tasks](https://docs.python.org/3/library/asyncio.html)
* [Asynchronous Programming with Python 3](https://community.nitrous.io/tutorials/asynchronous-programming-with-python-3)
