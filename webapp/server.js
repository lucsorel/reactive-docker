'use strict';

// creates the ExpressJS web server for the Wahls protocol with websocket support
var express = require('express'),
    webApp = express(),
    http = require('http').Server(webApp),
    httpPort = 3033;

// serves the webapp
webApp.use(express.static('www'));

// starts the web aplication server on the configured HTTP port
http.listen(httpPort, function() {
    console.log('listening on *:' + httpPort + '\nctrl+c to stop the app');
});

// creates a job puller
var zmq = require('zmq'),
    puller = zmq.socket('pull'),
    tcpUrl = '0.0.0.0:6969',
    // builds the connection point from the docker-compose environment or from standalone value
    connectionPoint = 'tcp://' + (process.env.PROBES_TCP_URL || tcpUrl);

console.log('pulling from ' + connectionPoint + '...');

puller.on('message', function(data) {
    var message = JSON.parse(data);
    console.log('received job ' + message.pid + ': ' + message.job);
});

puller.connect(connectionPoint);

// shuts the application down on low-level errors
function shutdown() {
    console.log('webApp servers are shutting down...');
    process.exit(1);
};
process.on('SIGINT', shutdown).on('SIGTERM', shutdown);
