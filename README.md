# Reactive Docker
An example of combining application containers with docker in a reactive environment.

## Build image from git
```bash
# builds the docker image
sudo su
service docker start
cd docker/from-git
docker build -t git/reactive-docker-webapp .

# runs the image
docker run -p 8080:3033 --name webapp -d git/reactive-docker-webapp

# checks the state and logs
docker ps
docker logs <container short id/image name>

# stops the container
docker stop <container short id/image name>
```

## Build image from source files
```bash
# builds the docker image
sudo su
service docker start
cd webapp
docker build -t files/reactive-docker-webapp .
cd ../probes
docker build -t files/reactive-docker-probes .

# runs the image
docker run -p 8080:3033 -p 6969:6969 --name webapp -d files/reactive-docker-webapp
docker run -p 8080:3033 -p 6969:6969 --name webapp files/reactive-docker-webapp
docker run -p 6969:6969 --name probes -d files/reactive-docker-probes
docker run -p 6969:6969 --name probes files/reactive-docker-probes
docker run -p 6969:6969 --name preprocess files/reactive-docker-preprocess

# checks the state and logs
docker ps
docker logs <container short id>

# stops the container
docker stop <container short id/generated name>

# removes the container
docker rm probes
```

## Docker compose
* runs the composed application
```bash
docker-compose up
```
* cleans the images up so that they can be rebuilt
```bash
docker rm $(docker ps -a -q) && docker rmi reactivedocker_webapp reactivedocker_probes reactivedocker_preprocess
```

# ZeroMQ
## In Ubuntu
* [install the library and NodeJS bindings](https://github.com/JustinTulloss/zeromq.node/wiki/Installation) via Chris Lea PPA:
```bash
# installs the zmq library and developer files (for the bindings)
sudo add-apt-repository -y ppa:chris-lea/zeromq
sudo add-apt-repository -y ppa:chris-lea/libpgm
sudo apt-get update
sudo apt-get install -y make gcc g++ python libzmq3 libzmq3-dev
```

## In Alpine Linux
```bash
# installs the zmq library and developer files (for the bindings)
sudo apk add --no-cache make gcc g++ python zeromq zeromq-dev
```

## ZeroMQ bindings in a NodeJS project
```bash
npm i -S zmq
```

# Send TCP messages
* via telnet
```bash
telnet localhost 6969
# quit with ctrl+], then q
```

* via netcat
```bash
nc localhost 6969
# quit with ctrl + c
```

# Further discussions
* prefer the use of Akka actors over data streams in Spark when [dealing with OLAP processes](http://blog.premium-minds.com/akka-to-the-rescue/) when the combinatory explosion would require the processing of >1k RDDs from when raw data event
* use the [NodeJS cluster](https://nodejs.org/dist/latest-v4.x/docs/api/cluster.html) module to benefit from multicore processors
* examples of [broker systems with ZeroMQ by Octo](http://blog.octo.com/delegation-de-taches-avec-zeromq/)
* [RxJava + Java8 pour sortir de l'enfer des Callback](https://www.youtube.com/watch?v=Jo5M_0y8Y0w&list)
* alternatives to ZeroMQ:
  * Libnetwork, a Go-based technology to connect containers, acquired by Docker in 2015 (see [Docker networking overview](http://filipv.net/2016/02/17/docker-networking-overview/))
